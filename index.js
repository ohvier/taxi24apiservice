const express = require("express");
const drivers = require("./routes/drivers");
const riders = require("./routes/riders");
const trips = require("./routes/trips");
const bodyParser = require("body-parser");
const orm = require("orm");
const app = express();

// app.use(bodyParser.json());
app.use(express.json());

app.use("/api/drivers", drivers);
app.use("/api/riders", riders);
app.use("/api/trips", trips);

app.use((err, req, res, next) => {
  res.json(err);
});

const port = process.env.PORT || 3000;
app.listen(port, () => console.log(`Listening on port ${port} ...`));
