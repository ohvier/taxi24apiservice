#!/bin/bash
database="taxis"
echo "Configuring database : $database"
dropdb -U node_user taxis 
echo "dropped database : $database"
createdb -U node_user taxis node_password

echo "created database : $database"
psql -U node_user taxis < ./bin/sql/taxis.sql

echo "$database db configured!"