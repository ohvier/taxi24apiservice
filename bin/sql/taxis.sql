CREATE TABLE drivers
(
    id serial,
    names character varying(50),
    phone character varying(50),
    gender character varying(50),
    status character varying(50),
    currentlocation character varying(100)
);

CREATE TABLE riders
(
    id serial,
    names character varying(50),
    phone CHARACTER varying(50),
    gender character varying(50),
    currentlocation character varying(100)
);

CREATE TABLE trip
(
    driver serial,
    rider serial,
    departure character varying(50),
    destination character varying(50),
    distance character varying(50),
    price character varying(50),
    createdAt character varying(50),
    updatedAt character varying(50),
    status character varying(50)
);

INSERT INTO drivers
    (names,phone,gender,currentlocation)
VALUES
    ('Jolie Kagoyire', '250732540635', 'F', 'Khana Khazana Kiyovu, 9 KN 31 St, Kigali'),
    ('Russie Muteteli', '250788489401', 'F', 'Repub Lounge, 16 KG 674 St, Kigal');

INSERT INTO riders
    (names,phone,gender,currentlocation)
VALUES

    ('Charles Wilkinson', '1-545-794-1417 x609', 'M', 'Pili Pili, KG 303 St, Kigali'),
    ('Electa Prosacco', '1-959-598-5510', 'M', 'German Butchery, KN 61 St, Kigali'),
    ('Warren Koss', '1-082-339-0712 x664', 'M', 'Sole Luna, KG 599 Street, Kigali'),
    ('Haskell Ziemann', '006-422-7631', 'M', 'Now Now Rolex, 2 KG 2 Ave, Kigali'),
    ('Mittie Haag', '1-050-817-9754 x497', 'M', 'Afrika Bite, 7 KG 674 St, Kigali'),
    ('Terrence Schiller', '941-699-3331', 'M', 'Hôtel Chez Lando, KG 201 St, Kigali'),
    ('Sylvan Howe', '452.286.9672', 'M', 'Ubumwe Grande Hotel, Kigali'),
    ('Alec Kilback', '1-917-709-4071 x833', 'M', 'Grand Legacy Hotel, KN 3 Rd, Kigali'),
    ('Maverick Bauch', '614-319-0041', 'M', 'Kigali International Airport, KN 5 Rd, Kigali'),
    ('Amelia Heidenreich', '1-202-615-6000', 'M', 'Work (Bank of Kigali Headquarters)'),
    ('Julie Bruen', '516-693-9792', 'M', 'Lebanon Hotel Kigali, KG 213 street, 58 KG 233 St, Kigali'),
    ('Daija Fahey', '(538) 080-4715 x1684', 'M', 'La Palisse Hotel, Kigali, KK 3 Rd, Kigali'),
    ('Emely McLaughlin', '799.410.5230', 'M', 'Kigali Genocide Memorial, KG 14 Ave, Kigali'),
    ('Emmalee Gerlach', '235-172-5652 x995', 'M', 'Kigali arena, KG 17 Ave, Kigali'),
    ('Birdie Langosh', '(172) 578-7434 x135', 'M', 'Intare Conference Arena, Nyagahinga, Kabuga'),
    ('Bo Predovic', '(937) 054-5073', 'M', 'Bourbon Coffee, KN 4 Ave, Kigali'),
    ('Annabelle Ritchie', '(015) 532-3511 x870', 'M', 'The Manor Hotel, KG 552 St, Kigali');

