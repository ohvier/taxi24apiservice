## Taxi24 Coding Exercise

Taxi24 is a new startup based in Kigali, providing a white-label solution to the
existing taxi companies and hotels.
Practically, it is a set of APIs that other companies can use to manage their fleet of drivers and
allocate drivers to passengers.

### Tools and Versions

Node v10.15.1

Postgress v11.5

### Prerequisites

To run the system you need to have nodeJs, Postgres DB installed on your system.

### Setup Process

To run the app, enter the commands below in terminal.

```
git clone https://gitlab.com/ohvier/taxi24apiservice.git

cd taxi24apiservice

npm install

nodemon

```

### JSON API End Points

Open your favourite REST API tester application (mine is Postman) and open the following URLs:

#### To get a list of all drivers

```
http://localhost:3000/api/drivers

```

When this endpoint is called using a GET method the app would fetch data from the postgress database and return a response of driver objects in json format
as shown below.

```json
[
  {
    "id": 3,
    "names": "Dieu Donne Nyakayiro",
    "phone": "250788444494",
    "gender": "M",
    "status": "AVAILABLE",
    "currentlocation": "Brachetto, KG 5 Ave, Kigali"
  },
  {
    "id": 4,
    "names": "J P Butare",
    "phone": "250788487494",
    "gender": "M",
    "status": "AVAILABLE",
    "currentlocation": "Lemigo Hotel, KG 624 St, Kigali"
  }
]
```

#### To get a list of all available drivers

```
http://localhost:3000/api/drivers/available

```

When this endpoint is called using a GET method the app would fetch data from the postgress database and return a response of all available drivers objects array

#### To get a list of all available drivers within 3km for a specific location

```
http://localhost:3000/api/drivers/available/proximity

```

When this endpoint is called using a POST method the app would fetch data from the postgress database and return a response of all available drivers in 3 km from
a given location objects array

**REQUEST**

```json
{
  "origins": "Lemigo Hotel, KG 624 St, Kigali"
}
```

#### To get a specific driver by ID

```
http://localhost:3000/api/drivers/id

```

When this endpoint is called using a GET method the app would fetch data from the postgress database and return a response of the driver whose ID is in the URL parameters

#### To create a new Trip by assigning a driver to a rider

```
http://localhost:3000/api/trips

```

This endpoint is called using a POST method

**REQUEST**

```json
{
  "driver": "250788425202",
  "rider": "250788444494",
  "departure": "Kinamba",
  "destination": "Airport",
  "distance": "7 km",
  "price": "5000",
  "starttime": "today",
  "endtime": "today"
}
```

**RESPONSE**

```json
{
  "driver": "250788425202",
  "rider": "250788444494",
  "departure": "Kinamba",
  "destination": "Airport",
  "distance": "7 km",
  "price": "5000",
  "starttime": "today",
  "endtime": "today",
  "status": "ACTIVE",
  "id": 5
}
```

#### To complete a Trip

```
http://localhost:3000/api/trips/8

```

This endpoint is called using a PUT method the app would set the trip with ID supplied in the parameters and the driver's status will be set to AVAILABLE

#### To get a list of all active Trips

```
http://localhost:3000/api/trips

```

This endpoint is called using a GET method the app would fetch data from the postgress database and return a response of all available trips objects array

#### To get a list of all riders

```
http://localhost:3000/api/riders

```

This endpoint is called using a GET method the app would fetch data from the postgress database and return a response of all riders objects array

#### To get a specific driver by ID

```
http://localhost:3000/api/riders/id

```

This endpoint is called using a GET method the app would fetch data from the postgress database and return a response of the rider whose ID is in the URL parameters

#### To get a specific driver by ID

```
http://localhost:3000/api/riders/closest/limit

```

This endpoint is called using a POST method the app would fetch data from the postgress database and return a response of the ri3 closest drivers to a rider's location
