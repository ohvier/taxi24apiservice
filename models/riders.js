const Sequelize = require("sequelize");
const sequelize = require("../db/sequelize");
const Trip = require("../models/trips");

const Riders = sequelize.define("riders", {
  names: {
    type: Sequelize.STRING
  },
  phone: {
    type: Sequelize.STRING,
    primaryKey: true
  },
  phone: {
    type: Sequelize.STRING
  },
  gender: {
    type: Sequelize.CHAR
  },
  status: {
    type: Sequelize.CHAR
  },
  currentlocation: {
    type: Sequelize.STRING
  }
});

Riders.hasOne(Trip);

module.exports = Riders;
