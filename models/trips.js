const Sequelize = require("sequelize");
const sequelize = require("../db/sequelize");
const Drivers = require("../models/drivers");

const Trip = sequelize.define("trip", {
  id: {
    type: Sequelize.INTEGER,
    primaryKey: true
  },
  driver: {
    type: Sequelize.STRING
  },
  rider: {
    type: Sequelize.STRING
  },
  departure: {
    type: Sequelize.STRING
  },
  destination: {
    type: Sequelize.CHAR
  },
  distance: {
    type: Sequelize.CHAR
  },
  price: {
    type: Sequelize.STRING
  },
  createdAt: {
    type: Sequelize.DATE
  },
  updatedAt: {
    type: Sequelize.DATE
  },
  status: {
    type: Sequelize.STRING
  }
});

module.exports = Trip;
