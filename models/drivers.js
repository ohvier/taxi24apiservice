const Sequelize = require("sequelize");
const sequelize = require("../db/sequelize");
const Trip = require("../models/trips");

const Drivers = sequelize.define("drivers", {
  names: {
    type: Sequelize.STRING
  },
  phone: {
    type: Sequelize.STRING,
    primaryKey: true
  },
  phone: {
    type: Sequelize.STRING
  },
  gender: {
    type: Sequelize.CHAR
  },
  status: {
    type: Sequelize.CHAR
  },
  currentlocation: {
    type: Sequelize.STRING
  }
});

Drivers.hasOne(Trip);

module.exports = Drivers;
