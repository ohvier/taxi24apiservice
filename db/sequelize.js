const Sequelize = require("sequelize");

const {
  user,
  host,
  database,
  password,
  port
} = require("../secrets/db_configuration");

const sequelize = new Sequelize(
  `postgres://${user}:${password}@${host}:${port}/${database}`
);

sequelize
  .authenticate()
  .then(() => {
    console.log("connected to postgres");
  })
  .catch(err => {
    console.error("unable to connect :" + err);
  });

module.exports = sequelize;
