const express = require("express");
const pool = require("../db");
const router = express.Router();

const Riders = require("../models/riders");
const Drivers = require("../models/drivers");

const distance = require("../distance/trajectory");
const redis = require("../db/redis");

//get route to return a list of all riders

router.get("/", async (req, res) => {
  Riders.findAll({
    attributes: ["names", "gender", "currentlocation"]
  }).then(riders => {
    res.json(riders);
  });
});

//get route to return 3 closest drivers to a specific rider

router.post("/closest/:limit", async (req, res) => {
  let origins = [req.body.origins];

  const limit = req.params.limit;
  Drivers.findAll({
    attributes: ["names", "status", "gender", "currentlocation"],
    where: {
      status: "AVAILABLE"
    },
    limit: limit
  }).then(drivers => {
    let destinations = [];
    var parsedJSON = JSON.parse(JSON.stringify(drivers));
    for (var i = 0; i < parsedJSON.length; i++) {
      destinations.push(parsedJSON[i].currentlocation);
    }

    const input = { origins, destinations };
    console.log(input);
    let distA = async () => {
      distance(input)
        .then(response => {
          console.log(response);
        })
        .catch();
    };

    var parsedJSON2 = parseDistances(JSON.parse(JSON.stringify(distA())));

    console.log(">>>" + parsedJSON2.catch());

    res.send(drivers);
  });
});

const parseDistances = async parsedJ => {
  let closest = [];
  //  var parsedJ1 = JSON.parse(parsedJ);
  for (var i = 0; i < parsedJ.length; i++) {
    closest.push(parsedJ[i].duration);
  }
  return closest;
};

//get route to return a specific rider by ID it could be a phone number

router.get("/:id", async (req, res, next) => {
  const { id } = req.params;
  Riders.findAll({
    attributes: ["names", "gender", "currentlocation"],
    where: {
      id
    }
  }).then(riders => {
    res.json(riders);
  });
});

module.exports = router;
