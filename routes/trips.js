const express = require("express");
const pool = require("../db");
const router = express.Router();
const Trip = require("../models/trips");
const Drivers = require("../models/drivers");

const currentDate = new Date();

//post route to create a trip

router.post("/", async (req, res, next) => {
  const status = "ACTIVE";
  const createdAt = currentDate;
  const updatedAt = "";
  const { driver, rider, departure, destination, distance, price } = req.body;

  Trip.create({
    driver,
    rider,
    departure,
    destination,
    distance,
    price,
    createdAt,
    updatedAt,
    status
  })
  .then(trip => {
    res.json(trip);
  });
});

//put method to complete a trip
router.put("/:id", async (req, res) => {
  const endtime = currentDate;
  const status = "ENDED";
  const { id } = req.params;

  pool.query(
    "UPDATE trip set status=($1),endtime=($2) WHERE id=($3)",
    [status, endtime, id],
    (err, result) => {
      if (err) console.log(err);
    }
  );
});

//get route to return all active trips
router.get("/", async (req, res, next) => {
  Trip.findAll({
    attributes: [
      "id",
      "driver",
      "rider",
      "departure",
      "destination",
      "status",
      "createdAt",
      "updatedAt"
    ],
    where: {
      status: "ACTIVE"
    }
  }).then(trip => {
    res.json(trip);
  });
});

module.exports = router;
