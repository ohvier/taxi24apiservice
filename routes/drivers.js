const express = require("express");

const Drivers = require("../models/drivers");
const Sequelize = require("sequelize");
const distance = require("../distance/trajectory");
const redis = require("../db/redis");

const router = express.Router();
const Op = Sequelize.Op;
//get route to return a list of all drivers of the system

router.get("/", async (req, res, next) => {
  Drivers.findAll({
    attributes: ["names", "status", "gender", "currentlocation"]
  }).then(drivers => {
    res.json(drivers);
  });
});

//get route to return all available drivers i.e who are not on trip currently

router.get("/available", async (req, res, next) => {
  Drivers.findAll({
    attributes: ["names", "status", "gender", "currentlocation"],
    where: {
      status: "AVAILABLE"
    }
  }).then(drivers => {
    res.json(drivers);
  });
});

//get route to return all drivers available in 3 kms from a given location

router.post("/available/:proximity", async (req, res, next) => {
  let origins = [req.body.origins];

  Drivers.findAll({
    attributes: ["names", "status", "gender", "currentlocation"],
    where: {
      status: "AVAILABLE"
    }
  }).then(drivers => {
    let destinations = [];
    var parsedJSON = JSON.parse(JSON.stringify(drivers));
    for (var i = 0; i < parsedJSON.length; i++) {
      destinations.push(parsedJSON[i].currentlocation);
    }

    const input = { origins, destinations };
    console.log(input);
    let distA = async () => {
      distance(input)
        .then(response => {
          console.log(response);
        })
        .catch();
    };

    var parsedJSON2 = parseDistances(JSON.parse(JSON.stringify(distA())));

    console.log(">>>" + parsedJSON2.catch());

    res.send(drivers);
  });
});

const parseDistances = async parsedJ => {
  let closest = [];
  //  var parsedJ1 = JSON.parse(parsedJ);
  for (var i = 0; i < parsedJ.length; i++) {
    if (parsedJ[i].distanceValue < 3000) {
      closest.push(parsedJ[i].duration);
    }
  }
  return closest;
};

//get route to return a specific driver by ID (could be a phone number)

router.get("/:id", async (req, res, next) => {
  const { id } = req.params;
  Drivers.findAll({
    attributes: ["names", "status", "gender", "currentlocation"],
    where: {
      id: id
    }
  }).then(drivers => {
    res.json(drivers);
  });
});

module.exports = router;
