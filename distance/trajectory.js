const distance = require("google-distance");
const { MAPKEY } = require("../secrets/db_configuration");

distance.apiKey = MAPKEY;

const getDistance = input => {
  return new Promise((resolve, reject) => {
    distance.get(input, function(err, data) {
      if (err) reject(err);
      resolve(data);
    });
  });
};

module.exports = getDistance;
