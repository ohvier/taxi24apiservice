const redis = require("../db/redis");
const getDistance = require("./trajectory");

const origins = ["Lemigo Hotel, KG 624 St, Kigali"];
const destinations = [
  "Brachetto, KG 5 Ave, Kigali",
  "MTN Centre, KG 9 Ave, Kigali"
];

const input = { origins, destinations };

const x = async () => {
  getDistance(input)
    .then(response => {
      console.log(response);
    })
    .catch();
};

x();
